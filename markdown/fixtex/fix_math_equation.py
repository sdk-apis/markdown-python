
import re

pattern_math_equation = re.compile(r'\$\$([\s\S]*?)\$\$' )
# 针对 $$\sum_i  \n f  dx$$ 之类的数学表达式


def func_handle_replace_math_equation(matchobj):
    urlr = matchobj.group(0)
    print('得到 ', urlr)
    # "sdsd"
    #imgid =  urlr.split('=')[-1][:-1]
    #uri  =  '"http://static-gitbay.gitbay.net/weixiaozhan/wxapp/Img/%s/"' % imgid
    uri ="""\n%s\n""" % urlr
    return uri


def add_new_line_to_math_equation(inputStr=''):
    replacedStr = re.sub( pattern_math_equation  , func_handle_replace_math_equation , inputStr)
    #print(replacedStr)
    ## 下面的这个是会连成一整块
    #y=pattern_math_equation.findall( inputStr )
    return replacedStr









def test():
    import markdown
    your_text_string="""
    this is a
    $$
    \Big| \sum_i    \big\{  \nabla_{i} f( X+   [Y-X]\cdot \tau  ) -\nabla_{i} f( X   )  \big\} \cdot  ( y_i-x_i ) \Big|  \\
    \leq
     \Big( \sum_i  \big|  \nabla_{i} f( X+   [Y-X]\cdot \tau  ) -\nabla_{i} f( X   )  \big|^p \Big)^{1/p}
     \cdot
     \Big( \sum_i  \big| y_i-x_i  \big|^q \Big)^{1/q}
    $$
    其中  $\frac{1}{p}+ \frac{1}{q}=1 $. 上面的也可以简化写成
    $$
    \Big| \left \langle  \nabla  f( X+   [Y-X]\cdot \tau  ) -\nabla  f( X   ) , Y-X \right \rangle \Big|
    \leq
     \big\|  \nabla  f( X+   [Y-X]\cdot \tau  ) -\nabla  f( X   ) \big\|_p \cdot \big\| Y-X \big\|_q
    $$
    由于   f 满足  Lipschitz continuous  gradient ： $\|\nabla f(X) -\nabla f(Y) \|_p \leq L \|  X-Y\|_q $  ,   $\forall \  x,y \in dom(f)$   ， then

    阿斯顿发
    $$
    \Big| \left \langle  \nabla  f( X+   [Y-X]\cdot \tau  ) -\nabla  f( X   ) , Y-X \right \rangle \Big|
    \leq
     \big\|  \nabla  f( X+   [Y-X]\cdot \tau  ) -\nabla  f( X   ) \big\|_p \cdot \big\| Y-X \big\|_q
    $$
    AAS
    """
    #
    replacedStr=add_new_line_to_math_equation(inputStr=your_text_string)
    print(replacedStr)
    html = markdown.markdown(replacedStr)
    print(html )


#
