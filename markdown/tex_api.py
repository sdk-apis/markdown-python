import re

################################################
#  修复公式
################################################

pattern_math_equation = re.compile(r'\$\$([\s\S]*?)\$\$' )
# 针对 $$\sum_i  \n f  dx$$ 之类的数学表达式


def func_handle_replace_math_equation(matchobj):
    urlr = matchobj.group(0)
    print('得到 ', urlr)
    # "sdsd"
    #imgid =  urlr.split('=')[-1][:-1]
    #uri  =  '"http://static-gitbay.gitbay.net/weixiaozhan/wxapp/Img/%s/"' % imgid
    uri ="""\n%s\n""" % urlr
    return uri


def add_new_line_to_math_equation(inputStr=''):
    replacedStr = re.sub( pattern_math_equation  , func_handle_replace_math_equation , inputStr)
    #print(replacedStr)
    ## 下面的这个是会连成一整块
    #y=pattern_math_equation.findall( inputStr )
    return replacedStr


################################################
#  修复行内inline表达式公式
################################################

pattern_math_inline_blank = re.compile(r'\$.*?\x20\$' )
# 针对 $\sum_i  \n f  dx $ 之类的带空格的数学表达式

pattern_math_inline_top_blank = re.compile(r'\$\x20.*?\$' )
# 针对 $ \sum_i  \n f  dx$ 之类的带空格的数学表达式


## 针对 $\sum_i  \n f  dx $ 之类的带空格的数学表达式
def func_handle_replace_math_inline_blank(matchobj):
    text_string = matchobj.group(0)
    #print('得到 ', text_string)
    result_string = text_string[:-1].rstrip()+'$'
    #
    return result_string



# 针对 $ \sum_i  \n f  dx$ 之类的带空格的数学表达式
#
def func_handle_replace_math_inline_top_blank(matchobj):
    text_string = matchobj.group(0)
    #print('得到 ', text_string)
    result_string = '$'+ text_string[1:].lstrip()
    #
    return result_string



def delete_blank_to_math_inline(inputStr=''):
    replacedStr = re.sub( pattern_math_inline_blank  , func_handle_replace_math_inline_blank , inputStr)
    replacedStr = re.sub( pattern_math_inline_top_blank  , func_handle_replace_math_inline_top_blank , replacedStr)
    #print(replacedStr)
    ## 下面的这个是会连成一整块
    #y=pattern_math_inline_blank.findall( inputStr )
    return replacedStr


################################################
#  main 入口
################################################


def  on_new_tex_string(input_str=''):
    # 添加新的换行
    str_v1 = add_new_line_to_math_equation(input_str)
    # 删除空格
    replacedStr = delete_blank_to_math_inline(inputStr=str_v1)
    return replacedStr
    #return input_str
